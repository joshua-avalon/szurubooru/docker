# Szurubooru Docker

This is the docker repository of Szurubooru..

To know more about Szurubooru,  please see the [original][szurubooru] or the [upstream repository][upstream].

The repository contains 3 docker image: [joshava/szurubooru], joshava/szurubooru-api (TBD), joshava/szurubooru-client (TBD)

## Difference between rr-/szurubooru

* Server and client are in single image.
* Based on Python 3.7 and Alpine instead on Python 3.6 and Slim.
* Support S3 as image storage.
* CORS headers for api.
* Use uWSGI instead of waitress.
* Small CSS changes.

## joshava/szurubooru

[![Docker Pull](https://img.shields.io/docker/pulls/joshava/szurubooru.svg)][joshava/szurubooru]
[![Docker Star](https://img.shields.io/docker/stars/joshava/szurubooru.svg)][joshava/szurubooru]
[![Image Size](https://img.shields.io/microbadger/image-size/joshava/szurubooru.svg)][joshava/szurubooru]
[![Image Layer](https://img.shields.io/microbadger/layers/joshava/szurubooru.svg)][joshava/szurubooru]

This is a image that contains **both** api and client.

### Usage

Please read the [original tutorial][install] for original configurations.
Here is the new configurations.

* `S3_DOMAIN`: The should be the bucket domain, `bucket.s3.amazonaws.com`. The content should be public readable (listing is not need).
* `S3_ENDPOINT`: Only needed if you are not using Amazon. `https://sfo2.digitaloceanspaces.com`
* `S3_REGION`: Bucket region.
* `S3_ACCESS_KEY`: Bucket access key.
* `S3_ACCESS_SECRET`: Bucket access secret.

#### Docker Compose

Please see the [example](docker/docker-compose.yml).


## Build

The Docker images are built by GitLab CI. You can refer to [.gitlab-ci.yml](.gitlab-ci.yml) on how to build.

It is recommend to use the pre-build image, because it takes a long time to build Python packages.

You can check the build process in the [pipelines tab][pipelines].

[szurubooru]: https://github.com/rr-/szurubooru
[upstream]: https://gitlab.com/joshua-avalon/szurubooru/upstream
[docker]: https://gitlab.com/joshua-avalon/szurubooru/docker
[joshava/szurubooru]: https://hub.docker.com/r/joshava/szurubooru
[pipelines]: https://gitlab.com/joshava/szurubooru/pipelines
[install]: https://gitlab.com/joshua-avalon/szurubooru/upstream/blob/master/INSTALL.md
