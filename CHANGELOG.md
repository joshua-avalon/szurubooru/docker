# Changelog

All notable changes to this project will be documented in this file.

## 2.0.0
### Changed
* Migrate to [joshua-avalon/szurubooru/docker][repository].

[repository]: https://gitlab.com/joshua-avalon/szurubooru/docker
