FROM alpine as client

ARG CLIENT_VERSION=1.3.2

WORKDIR /client
WORKDIR /app

RUN apk add --no-cache wget zip
RUN wget -O client.zip "https://gitlab.com/joshua-avalon/szurubooru/client/-/jobs/artifacts/${CLIENT_VERSION}/download?job=release"
RUN unzip client.zip
RUN mv public/* /client

FROM alpine as server

ARG SERVER_VERSION=1.1.3

WORKDIR /server
WORKDIR /app

RUN apk add --no-cache git
RUN git clone https://gitlab.com/joshua-avalon/szurubooru/server.git . && \
    git checkout "${SERVER_VERSION}"
RUN mv alembic.ini wait-for-es generate-thumb config.yaml.dist requirements.txt szurubooru /server

FROM joshava/uwsgi-nginx

WORKDIR /cache

WORKDIR /client
COPY --from=client /client/ ./

WORKDIR /server

COPY --from=server /server/ ./

RUN apk add --no-cache ffmpeg postgresql-libs jpeg-dev zlib-dev && \
    apk add --no-cache --virtual .build-deps build-base gcc musl-dev postgresql-dev libffi-dev && \
    pip install --no-cache-dir -r ./requirements.txt && \
    apk --purge del .build-deps

COPY docker/root/ /
